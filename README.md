# DAINF

[Site de deploy](https://dainf-utfpr-pb.gitlab.io/web/)

[Site original](http://www.pb.utfpr.edu.br/favarim/dainf/)

## Editando

0b00.  Faça o download

0b01.  Instale NodeJS, com suporte a nmp e npx

0b10  Use `npm install` no console, sob o diretório que contém o `package.json`

0b11.  Use `npm start` para iniciar o hosting local

## Compilando

0b00. Use './deploy.sh' para gerar o public e fazer o push

Ah, you're respectable, then?

0b00. Apague a pasta public, acima do diretório source (./public)

0b01. Use `npm run build` no console, sob o diretório que contém o 'package.json'

0b10. Mova a gerada pasta build para o lugar da antiga public (mv ./source/build ./public)

Ao fazer o commit, o site será testado e disponibilizado

## [Documentação original](https://demos.creative-tim.com/paper-kit-react/#/documentation/introduction?ref=pkr-github-readme).

## Recursos (repositório original)

- Demo: <http://demos.creative-tim.com/paper-kit-react/#/index?ref=pkr-github-readme>

- Licença: <https://www.creative-tim.com/license?ref=pkr-github-readme>

- Suporte: <https://www.creative-tim.com/contact-us?ref=pkr-github-readme>

- Issues: [Github Issues Page](https://github.com/creativetimofficial/paper-kit-react/issues)

## Links úteis

- [Tutoriais](https://www.youtube.com/channel/UCVyTG4sCw-rOvB9oHkzZD1w)
