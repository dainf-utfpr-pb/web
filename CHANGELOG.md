# Change Log

Mudanças em funções, documentação, e realocação de arquivos vão aqui.

- IndexHeader.js movido de components/Headers/ para components/Home/Header.js
- IndexNavbar.js movido de components/Navbars/ para components/Common/Navbar.js
- DemoFooter.js movido de components/Footers/ para components/Common/Footer.js