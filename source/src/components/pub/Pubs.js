import React from "react"

import Navbar from "components/common/Navbar.js"
import Footer from "components/common/Footer.js"

import "assets/css/Pubs.css"

//Compõe a página com todas as notícias geradas (atualmente, 3). Exibe também uma pequena coluna à direita com informação de autores

function Teste (props) {
	return (
		<div>
			<div className="row">
				<div className="leftcolumn">
					<div className="card">
						<h2>{props.title}</h2>
						<h5>{props.description}, {props.date}</h5>
						<div
							className="fakeimg"
							style={{ height: "200px" }}
						>
							Image
						</div>
						<p>{props.about}</p>
						<p>{props.text}</p>
					</div>
				</div>

				<div className="rightcolumn">
					<div className="card">
						<h2>Autor</h2>
						<div
							className="imagem"
							style={{ height:"100px" }}
						>
							Image
						</div>
						<p>{props.aboutauthor}</p>
					</div>
				</div>
			</div>
		</div>
	);
}

export default Teste;