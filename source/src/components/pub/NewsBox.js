import React from "react";

import {
	Card,
	CardText,
	CardTitle,
	CardBody,
} from 'reactstrap';

//Gera, na página de noticias, um bloco que expõe a noticia por completo, como um folder

const NewsBox = (props) => {
	return (
		<Card style={{ maxWidth:"700px" }}>
			<img top width="700px" src={props.img} alt={props.subtitle} />
			<CardBody>
				<CardTitle>{props.title}</CardTitle>
				<CardText>{props.subtitle}</CardText>
				<CardText>{props.text}</CardText>
				<CardText>
					<small className="text-muted">Escrito por {props.author}</small>
				</CardText>
			</CardBody>
		</Card>
	);
}

export default NewsBox