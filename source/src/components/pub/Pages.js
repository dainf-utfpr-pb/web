import React from "react"
import { Pagination, PaginationItem } from "reactstrap"
import {Link} from "react-router-dom"

//Faz a paginação da página de notícias. Quando a página ficar extensa, esta função será útil para navergar entre várias páginas

const Pages = (props) => {
	return (
		<Pagination>

			<PaginationItem>
				<Link to="/noticias/last">
				Inicio
				</Link>
			</PaginationItem>

			{refer()}

			<PaginationItem>
				<Link to="/noticias/old">
				Fim
				</Link>
			</PaginationItem>

		</Pagination>
	);

	function refer(){
		let lista = []
		for (let i = (props.refs.length-1); i > -1; i--){
			lista.push(<PaginationItem><Link to={props.links[i]}>{props.refs[i]}</Link></PaginationItem>)
		}
		return lista;
	}
}

export default Pages;