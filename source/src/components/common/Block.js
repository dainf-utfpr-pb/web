import React from "react"
import "assets/css/custom.css"
import {Link} from "react-router-dom"

//Cria um bloco simples de informação: Título, um link e um titulo de link
function Block (props) {

	return (
		<nav className="block">
			<ul>
				<li>
					<h5>{props.title}</h5>
					{refer()}
				</li>
			</ul>
		</nav>
	);

	function refer(){
			let lista = []
			for (let i = 0; i < props.refs.length; i++) {
				lista.push(<LinkBranco to={props.links[i]}>{props.refs[i]}</Link>)
			}
			return lista
	}
}

export default Block;