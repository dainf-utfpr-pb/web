import React, { useState } from 'react';
import {Button, Collapse, ButtonGroup} from 'reactstrap';
import {Link} from "react-router-dom"
const ButtonCollapse = (props) => {

	const [isOpen, setIsOpen] = useState(false); //Inicializando variáveis de alternação

	const toggle = () => setIsOpen(!isOpen);

	return (
		<div>
			<Button className='botoes' onClick={toggle} style={{ width: "100%" } } caret> {/*Chamada do elemento botão a ser renderizado*/}
				{props.title} //Título
			</Button>
			<Collapse isOpen={isOpen}> {/*Fazer o collapse do botão para alternar*/}
				<ButtonGroup vertical> {/*Cria um grupo para renderizar vários botões juntos*/}
					{refer() //Renderiza n elementos simultaneamente }
				</ButtonGroup>
			</Collapse>
		</div>
	);

	function refer(){
		let lista = []
		for (let i = 0; i < props.refs.length; i++) { //Gera os links de cada botão. A String gerada fará a chamada do elemento
			lista.push(<Link className="LinkBranco" to={props.links[i]}><Button className='botoesBrancos'>{props.refs[i]}</Button></Link>)
		}
		return lista
	}
}

export default ButtonCollapse;