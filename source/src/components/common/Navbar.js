/*!

=========================================================
* Paper Kit React - v1.2.0
=========================================================

* Product Page: https://www.creative-tim.com/product/paper-kit-react

* Copyright 2020 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/paper-kit-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.*/


import React from "react";
// nodejs library that concatenates strings
import classnames from "classnames";
// reactstrap components
import {
	Button,
	NavbarBrand,
	Navbar,
	Container,
	Row
//	Col
} from "reactstrap";
import {Link} from "react-router-dom"

import "assets/css/custom.css"

import Dropdown from "components/common/Dropdown.js"

//Este apresenta a barra de nabegação no topo do site, com link de página inicial e contato, sendo subtituida em baixas resoluções pelo
//menu de dropdown

function IndexNavbar() {

	return (
		<Navbar className={classnames("fixed-top")} expand="lg">
			<Container>
				<Row>
					<Dropdown
						title="MENU"
					/>

						<NavbarBrand
							data-placement="bottom"
							title="Departamento de Informática"
						>
					<Link className="LinkPreto" to="/home">
							Página inicial
					</Link>
						</NavbarBrand>
				</Row>
					<Link to="/contato">
						<Button
							className="btn-round"
							color="danger"
						>
							Contato
						</Button>
					</Link>
			</Container>
		</Navbar>
	);
}

export default IndexNavbar;