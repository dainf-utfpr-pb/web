import React from 'react';
import {Button,  ButtonGroup} from 'reactstrap';
import {Link} from "react-router-dom"

//Gera a barra lateral de navegação, só funiconal em grandes resoluções, subtituindo o botão de menu/dropdown
const Leftbar = (props) => {
	return (
		<div style={{position: "sticky", top: "75px"}}>
			<ButtonGroup vertical className="menuLateral">
				<Button className="botoes" ><Link className="LeftBar" to="/corpodocente"><div className="divButtonLink">CORPO DOCENTE</div></Link></Button>
				<Button className="botoes" ><Link className="LeftBar" to="/graduacao"><div className="divButtonLink">GRADUAÇÃO</div></Link></Button>
				<Button className="botoes" ><Link className="LeftBar" to="/posgraduacao"><div className="divButtonLink">PÓS-GRADUAÇÃO</div> </Link></Button>
				<Button className="botoes" ><Link className="LeftBar" to="/pesquisa"><div className="divButtonLink">PESQUISA</div></Link></Button>
				<Button className="botoes" ><Link className="LeftBar" to="/extensao"><div className="divButtonLink">EXTENSÃO</div></Link></Button>
				<Button className="botoes" ><Link className="LeftBar" to="/infraestrutura"><div className="divButtonLink">INFRAESTRUTURA</div></Link></Button>
				<Button className="botoes" ><Link className="LeftBar" to="/documentos"><div className="divButtonLink">DOCUMENTOS</div></Link></Button>
				<Button className="botoes" ><Link className="LeftBar" to="http://www.pb.utfpr.edu.br/favarim/dainf/publicacoes"><div className="divButtonLink">PUBLICACÕES</div></Link></Button>
				<Button className="botoes" ><Link className="LeftBar" to="/noticias"><div className="divButtonLink">NOTICIAS/EVENTOS</div></Link></Button>
			</ButtonGroup>
		</div>
	);
}

export default Leftbar;