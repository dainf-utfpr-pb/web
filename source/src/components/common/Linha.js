
import React from "react";
import {Link} from "react-router-dom"
import "assets/css/tabelaCorpoDocente.css"

//Gera a linha da planilha de corpo docente
function Linha(props) {
	return (
		<div style={{width: "820px"}}>
            <div className="nome" > <Link className="hiperLink" to={props.link}>{props.nome}</Link> </div>
            <div className="titulacao" > {props.titulacao} </div>	
			<div className="regimedeTrabalho" > {props.regimedeTrabalho} </div>	
			<div className="email" > {props.email} </div>		
			<div className="lattes" > <a href={props.lattesLink}> <img src={require("assets/img/lattes.png")} alt=""/> </a> </div>	
	    </div>
	);
}

export default Linha;