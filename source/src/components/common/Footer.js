/*!

=========================================================
* Paper Kit React - v1.2.0
=========================================================

* Product Page: https://www.creative-tim.com/product/paper-kit-react

* Copyright 2020 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/paper-kit-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
/*eslint-disable*/
import React from "react";

// reactstrap components
import { Row, Container } from "reactstrap";

function Footer() {
	return (
		<footer className="footer" style={{borderTop: "5px solid #eee"}}>
			<Container >
				<Row>
					<nav>
						<ul>
							<a
								className="copyright"
								style={{ margin: '0px, 0px, 0px, 0px'}}
								href="https://www.google.com/maps/@-26.1958511,-52.6878164,3a,75y,243.97h,85.15t/data=!3m6!1e1!3m4!1sze-dd-V5Flcybj3KpST3Kw!2e0!7i13312!8i6656" 
								target="_blank"
							>
								<div ><p style={{fontSize: '20px', fontFamily: 'Arial', textTransform: 'none', textAlign: 'left', marginTop: '40px'}}>
									CÂMPUS PATO BRANCO:
									Via do Conhecimento, Km 1
									CEP: 85503-390 <br />
									Telefone: +55 (46) 3220-2511
									Fax: +55 (46) 3220-2500</p>
								</div>
							</a>
						</ul>
					</nav>
					<div className="ml-auto" />
					<nav className="footer-nav">
						<ul>
							<li>
								<a
									href="http://portal.utfpr.edu.br/home"
								>
									<img
										alt="..."
										className="utfprlogo"
										src={require("assets/img/utfpr-logo.png")}
										width="250"
									 />
								</a>
							</li>
						</ul>
					</nav>
				</Row>
			</Container>
		</footer>
	);
}

export default Footer;
