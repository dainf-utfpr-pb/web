import React from 'react';
import { Row, Col } from 'reactstrap';
//import {Link} from "react-router-dom"

const LayoutProfessor = (props) => {
	return (
		<div >
			<h3><strong>{props.Nome}</strong></h3>
			<hr/>
			<Row>
				<Col sm="6" md="4" lg="4" xl="2">
					<img src={props.Imagem} alt="" width="100%"/>
				</Col>
				<Col>
					<p className="colStyle"> <strong>{props.Atribuicoes}</strong> <br/>
						<strong>Email Institucional: </strong> {email(props.EmailInstitucional)} <br/>
						<strong>Email Alternativo: </strong>{email(props.EmailAlternativo)} <br/>
						<strong>Ramal: </strong>{props.Ramal} <br/>
						<strong>Sala: </strong>{props.Sala} <br/>
						<strong>Lattes: </strong> <a className="hiperLink" href={props.Lattes}>{props.Lattes}</a> <br/>
						<strong>Pagina: </strong> <a className="hiperLink" href={props.Pagina}>{props.Pagina}</a> <br/>
						<strong>Regime: </strong>{props.Regime} <br/>
						<strong>Ingresso: </strong>{props.Ingresso} <br/>
					</p>
				</Col>
			</Row>
			<Row>
				<Col>
					<p className="colStyle"> 
						<strong>Formação Acadêmica</strong> <br/>
						<ul>
							{refer(props.Formacao)}
						</ul> 
					</p>

					<p className="colStyle"> 
						<strong>Áreas de Intresse</strong> <br/>
						<ul>
							{refer(props.Interesses)}
						</ul> 
					</p> 
				</Col>
			</Row>
		</div>
	);

	function refer(item){
		let lista = []
		for (let i = 0; i < item.length; i++) {
			lista.push(<li>{item[i]}</li>)
		}
		return lista
	}

	function email(item){/*Esta função substitui o link de email por uma imagem de @. Diz o Favarim que engana bots...*/
		let ret

		const part01 = item.substring(0, item.indexOf("@"))
		const part02 = item.substring(item.indexOf("@") + 1)

		if(part01.length > 0 && part02.length > 0)
			ret = <i>{part01}<img src={require("assets/img/acirculo.png")} width="19px" alt=""/>{part02}</i>
		else {/*Para não ficar um @ quando não tem email*/}
			ret = <i>----</i>
		return ret
	}
}

export default LayoutProfessor;
