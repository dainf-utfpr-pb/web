import React, { useState } from 'react';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import {Link} from "react-router-dom"
const DropdownBt = (props) => {
//Faz uma botão deslizante que abre em subcategorias, Mas é só funcional em telas pequenas. Gera o leftbar de forma dinâmica
	const [dropdownOpen, setDropdownOpen] = useState(false);
	const toggle = () => setDropdownOpen(prevState => !prevState);

	return (
		<Dropdown isOpen={dropdownOpen} toggle={toggle} className='menuNavbar'>
			<DropdownToggle caret>
				{props.title}
			</DropdownToggle>
			<DropdownMenu modifiers={{setMaxHeight: {enabled: true,order: 890,fn: (data) => {return {...data,styles: {...data.styles,overflow: 'auto',maxHeight: '400px',},};},},}}>

				<Link to="/corpodocente"><DropdownItem >Corpo Docente</DropdownItem></Link>

				<DropdownItem divider /> {/*Cria um elemento do menu*/}
				<Link to="/graduacao"><DropdownItem >Graduação</DropdownItem></Link>
				<DropdownItem divider />

				<DropdownItem divider />
				<Link to="/posgraduacao"><DropdownItem >Pos-Graduação</DropdownItem></Link>
				<DropdownItem divider />

				<DropdownItem divider />
				<Link to="/pesquisa"><DropdownItem >Pesquisa</DropdownItem></Link>
				<DropdownItem divider />

				<DropdownItem divider />
				<Link to="/extensao"><DropdownItem >Extensão</DropdownItem></Link>
				<DropdownItem divider />

				<DropdownItem divider />
				<Link to="/infraestrutura"><DropdownItem >Infraestrutura</DropdownItem></Link>
				<DropdownItem divider />

				<Link to="/documentos"><DropdownItem >Documentos</DropdownItem></Link>
				<DropdownItem divider />

				<Link to="/publicacoes"><DropdownItem >Publicações</DropdownItem></Link>
				<DropdownItem divider />

				<Link to="/home"><DropdownItem >Noticias/Eventos</DropdownItem></Link>
				<DropdownItem divider />

			</DropdownMenu>
		</Dropdown>
	);
}

export default DropdownBt;