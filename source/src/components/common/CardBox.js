import React from "react";
import {Link} from "react-router-dom"
import {
	Card,
	CardImg,
	CardText,
	CardTitle,
	Button
} from 'reactstrap';

const CardBox = (props) => { //Cria uma caixa que comporta vários cards ao mesmo tempo (padrão do REACTSTRAP)
	return (
		<Card body style={{ borderRadius: '0px', margin: '5px'}}>
			<CardImg	{/*Imagem do card*/}
				top
				width="10px"
				src={props.img} {/*É importante que seja usado um require (nodejs) para o site interpretar a imagem como estando na engine do site*/}
				style={{ borderRadius: '0px', width: '100%', height: '100%'}}
			/>
			<CardTitle>{props.title}</CardTitle>
			<CardText>{props.text}</CardText>
			<Link to={props.link} >
				<Button>
					{props.refs}
				</Button>
			</Link>
		</Card>
	);
}

export default CardBox
