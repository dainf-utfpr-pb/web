/*!

=========================================================
* Paper Kit React - v1.2.0
=========================================================

* Product Page: https://www.creative-tim.com/product/paper-kit-react

* Copyright 2020 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/paper-kit-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
/*eslint-disable*/
import React from "react";

// reactstrap components
import { Container } from "reactstrap";

// core components

import "assets/css/bootstrap.min.css"
//import News from "components/home/News.js"

function IndexHeader() {
	return (
		<div>
			<div
				className="page-header section-dark"
				style={{
					backgroundImage:
					"url(" + require("assets/img/utfpr.jpg") + ")",
				}}
			>
				<div className="filter" />
				<div className="content-center">
					<Container>
						<div className="title-brand">
							<img
								alt="..."
								className="dainflogo"
								src={require("assets/img/dainfLogoBranco.png")}
								width="400"
							/>
						</div>
					</Container>
				</div>
			</div>
		</div>
	);
}

export default IndexHeader;
