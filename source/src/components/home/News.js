import React from "react"
import { CardGroup } from "reactstrap"
import CardBox from "components/common/CardBox.js"

var LastNews = [
	{...require("views/Content/Noticias/0/exemplo0/Info.js")},
	{...require("views/Content/Noticias/0/exemplo1/Info.js")},
	{...require("views/Content/Noticias/0/exemplo2/Info.js")}
]

//Apresenta um bloco de notícias na Home, que contém as noticias mais recentes do Site
//Composto de um titulo, um texto descritivo, uma imagem, titulo de link e link para o post da noticia
function News (props) {
	return (
		<div>
			<CardGroup tyle={{ margin: '20px'}}>
				{refer()}
			</CardGroup>
		</div>
	);
	function refer() {
		let lista = []
		for (let i = 0; i < LastNews.length; i++){
			lista.push(<CardBox
				title={LastNews[i].Title}
				text={LastNews[i].Description}
				img={require("views/Content/Noticias/0/exemplo"+[i]+"/img")}
				refs={LastNews[i].Ref}
				links={LastNews[i].Link}
			/>)
		}
		return lista
	}
}

export default News
