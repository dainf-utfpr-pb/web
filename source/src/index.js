/*!

=========================================================
* Paper Kit React - v1.2.0
=========================================================

* Product Page: https://www.creative-tim.com/product/paper-kit-react

* Copyright 2020 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/paper-kit-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
// Elementos de comportamento
import React from "react";
import ReactDOM from "react-dom";
import { Router, Route, Switch } from "react-router-dom";
import { createBrowserHistory } from "history";

// Estilos
import "assets/css/bootstrap.min.css";
import "assets/css/paper-kit.css";
import "assets/css/custom.css"


// Páginas de conteúdo
import Contato from "views/Content/Contato/Contato.js"
import CorpoDocente from "views/Content/CorpoDocente/CorpoDocente.js"
import Documentos from "views/Content/Documentos/Documentos.js"
import Codainf from "views/Content/Documentos/Codainf.js"
import Plenarias from "views/Content/Documentos/Plenarias.js"
import Resolucoes from "views/Content/Documentos/Resolucoes.js"
import Requerimentos from "views/Content/Documentos/Requerimentos.js"
import Extensao from "views/Content/Extensao/Extensao.js"
import Graduacao from "views/Content/Graduacao/Graduacao.js"
import Home from "views/Home.js";
import Infraestrutura from "views/Content/Infraestrutura/Infraestrutura.js"
import Pesquisa from "views/Content/Pesquisa/Pesquisa.js"
import PosGraduacao from "views/Content/PosGraduacao/PosGraduacao.js"
import Noticias from "views/Content/Noticias/Noticias.js"

// Páginas de professores
import AndreiaBeulke from "views/Content/CorpoDocente/Efetivos/AndreiaBeulke/andreiaBeulke.js"
import AdrianoSerckumecka from "views/Content/CorpoDocente/Efetivos/AdrianoSerckumecka/AdrianoSerckumecka.js"
import DalcimarCasanova from "views/Content/CorpoDocente/Efetivos/DalcimarCasanova/DalcimarCasanova.js"
import EdenDosciatti from "views/Content/CorpoDocente/Efetivos/EdenDosciatti/EdenDosciatti.js"
import EdilsonPontarolo from "views/Content/CorpoDocente/Efetivos/EdilsonPontarolo/EdilsonPontarolo.js"
import FabioFavarim from "views/Content/CorpoDocente/Efetivos/FabioFavarim/FabioFavarim.js"
import IvesPola from "views/Content/CorpoDocente/Efetivos/IvesPola/IvesPola.js"
import KathyaLinares from "views/Content/CorpoDocente/Efetivos/KathyaLinares/KathyaLinares.js"
import LucieneMarin from "views/Content/CorpoDocente/Efetivos/LucieneMarin/LucieneMarin.js"
import LuisBueno from "views/Content/CorpoDocente/Efetivos/LuisBueno/LuisBueno.js"
import MarceloTeixeira from "views/Content/CorpoDocente/Efetivos/MarceloTeixeira/MarceloTeixeira.js"
import MarcoBarbosa from "views/Content/CorpoDocente/Efetivos/MarcoBarbosa/MarcoBarbosa.js"
import MarcosMarini from "views/Content/CorpoDocente/Efetivos/MarcosMarini/MarcosMarini.js"
import MarizaDosciatti from "views/Content/CorpoDocente/Efetivos/MarizaDosciatti/MarizaDosciatti.js"
import PabloCavalcanti from "views/Content/CorpoDocente/Efetivos/PabloCavalcanti/PabloCavalcanti.js"
import RobisonBrito from "views/Content/CorpoDocente/Efetivos/RobisonBrito/RobisonBrito.js"
import ViniciusPegorini from "views/Content/CorpoDocente/Efetivos/ViniciusPegorini/ViniciusPegorini.js"
import VivianeSouza from "views/Content/CorpoDocente/Efetivos/VivianeSouza/VivianeSouza.js"
import ElianeFavero from "views/Content/CorpoDocente/Efetivos/ElianeFavero/ElianeFavero.js"
import LuciliaAraki from "views/Content/CorpoDocente/Efetivos/LuciliaAraki/LuciliaAraki.js"
import LuisRista from "views/Content/CorpoDocente/Efetivos/LuisRista/LuisRista.js"
import RubiaAscari from "views/Content/CorpoDocente/Efetivos/RubiaAscari/RubiaAscari.js"
import SilvioBoss from "views/Content/CorpoDocente/Efetivos/SilvioBoss/SilvioBoss.js"
import SoelaineAscari from "views/Content/CorpoDocente/Efetivos/SoelaineAscari/SoelaineAscari.js"
import GeriDutra from "views/Content/CorpoDocente/Efetivos/GeriDutra/GeriDutra.js"
import RichardsonRibeiro from "views/Content/CorpoDocente/Efetivos/RichardsonRibeiro/RichardsonRibeiro.js"


// others

var hist = createBrowserHistory();	//Faz o cache da página para não precisar recarregar

ReactDOM.render(
	<Router history={hist}>	{/*//elemento a ser usado de cache*/}
		<Switch>
			<Route //Estabelece a rota para o chamado da página
				path="/contato"	//Link de chamada
				component={Contato}	//Elemento a ser renderizado
			/>
			<Route
				path="/documentos/requerimentos" //Subpáginas precisam ser renderizadas antes da página pai
				component={Requerimentos}
			/>
			<Route
				path="/documentos/resolucoes"
				component={Resolucoes}
			/>
			<Route
				path="/documentos/codainf"
				component={Codainf}
			/>
			<Route
				path="/documentos/plenarias"
				component={Plenarias}
			/>
			<Route
				path="/documentos" //Página pai
				component={Documentos}
			/>
			<Route
				path="/extensao"
				component={Extensao}
			/>
			<Route
				path="/graduacao"
				component={Graduacao}
			/>
			<Route
				path="/infraestrutura"
				component={Infraestrutura}
			/>
			<Route
				path="/noticias/old"
				component={Noticias("last")}
			/>
			<Route
				path="/noticias/last"
				component={Noticias}
			/>
			<Route
				path="/noticias"
				component={Noticias}
			/>
			<Route
				path="/pesquisa"
				component={Pesquisa}
			/>
			<Route
				path="/posgraduacao"
				component={PosGraduacao}
			/>

			<Route
				path="/corpodocente/andreiaBeulke"
				component={AndreiaBeulke}
			/>

			<Route
				path="/corpodocente/adrianoSerckumecka"
				component={AdrianoSerckumecka}
			/>

			<Route
				path="/corpodocente/dalcimarCasanova"
				component={DalcimarCasanova}
			/>

			<Route
				path="/corpodocente/edenDosciatti"
				component={EdenDosciatti}
			/>

			<Route
				path="/corpodocente/edilsonPontarolo"
				component={EdilsonPontarolo}
			/>

			<Route
				path="/corpodocente/fabioFavarim"
				component={FabioFavarim}
			/>

			<Route
				path="/corpodocente/ivesPola"
				component={IvesPola}
			/>

			<Route
				path="/corpodocente/kathyaLinares"
				component={KathyaLinares}
			/>

			<Route
				path="/corpodocente/lucieneMarin"
				component={LucieneMarin}
			/>

			<Route
				path="/corpodocente/luisBueno"
				component={LuisBueno}
			/>

			<Route
				path="/corpodocente/marceloTeixeira"
				component={MarceloTeixeira}
			/>

			<Route
				path="/corpodocente/marcoBarbosa"
				component={MarcoBarbosa}
			/>

			<Route
				path="/corpodocente/marcosMarini"
				component={MarcosMarini}
			/>

			<Route
				path="/corpodocente/marizaDosciatti"
				component={MarizaDosciatti}
			/>

			<Route
				path="/corpodocente/pabloCavalcanti"
				component={PabloCavalcanti}
			/>

			<Route
				path="/corpodocente/robisonBrito"
				component={RobisonBrito}
			/>

			<Route
				path="/corpodocente/viniciusPegorini"
				component={ViniciusPegorini}
			/>

			<Route
				path="/corpodocente/vivianeSouza"
				component={VivianeSouza}
			/>

			<Route
				path="/corpodocente/elianeFavero"
				component={ElianeFavero}
			/>

			<Route
				path="/corpodocente/luciliaAraki"
				component={LuciliaAraki}
			/>

			<Route
				path="/corpodocente/luisRista"
				component={LuisRista}
			/>

			<Route
				path="/corpodocente/rubiaAscari"
				component={RubiaAscari}
			/>

			<Route
				path="/corpodocente/silvioBoss"
				component={SilvioBoss}
			/>

			<Route
				path="/corpodocente/soelaineAscari"
				component={SoelaineAscari}
			/>

			<Route
				path="/corpodocente/geriDutra"
				component={GeriDutra}
			/>

			<Route
				path="/corpodocente/richardsonRibeiro"
				component={RichardsonRibeiro}
			/>
			<Route
				path="/corpodocente"
				component={CorpoDocente}
			/>
			<Route
				path="/"
				component={Home}
			/>
		</Switch>
	</Router>,
	document.getElementById("root")
);
