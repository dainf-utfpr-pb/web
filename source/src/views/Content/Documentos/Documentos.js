import React from "react"

import Navbar from "components/common/Navbar.js";
import Footer from "components/common/Footer.js";
import Leftbar from "components/common/Leftbar.js";
import Header from "components/home/Header.js";

import {Link} from "react-router-dom"
import { Container, Row, Col } from 'reactstrap';

import "assets/css/custom.css"


function PaginaModelo (props){
	return (
		<div className="edit">
			<Navbar />
			<Header />
			<Container>
				<Row>
					<Col lg='3' className='menuLateral'>
						<Leftbar/>
					</Col>
					<Col md='12' lg='9' className="colStyle">

						<h2><strong> Documentos </strong></h2>
						<hr/>

						<Link className="hiperLink" to="/documentos/codainf">Atas CODAINF</Link><br/>
						<hr/>

						<Link className="hiperLink" to="/documentos/plenarias">Atas Plenaria</Link><br/>
						<hr/>

						<Link className="hiperLink" to="/documentos/requerimentos">Requerimentos</Link><br/>
						<hr/>

						<Link className="hiperLink" to="/documentos/resolucoes">Resoluções</Link><br/>
						<hr/>

						{/*<Link to={require("./Arquivos/manual.pdf")}><strong>Manual do Professor</strong></Link><br/>
						Esse manual visa auxiliar o professor da área de informática em relação ao conhecimento dos sistemas existentes da universidade, assim como apresentar de forma resumida alguns procedimentos existentes na instituição. 
						<hr/>

						<Link to={require("./Arquivos/plano")}><strong>Plano de Substituição de Aulas</strong></Link><br/>
						Modelo de Plano de Substituição/Reposição de Aulas.*/}
						<br/>
						<br/>
					</Col>
				</Row>
			</Container>
			<Footer />
		</div>
	);
}

export default PaginaModelo;