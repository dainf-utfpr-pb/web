import React from "react"

import Navbar from "components/common/Navbar.js";
import Footer from "components/common/Footer.js";
import Leftbar from "components/common/Leftbar.js";
import Header from "components/home/Header.js";

import { Container, Row, Col } from 'reactstrap';
import {Link} from "react-router-dom"
import "assets/css/custom.css"


function PaginaModelo (props){
	return (
		<div className="edit">
			<Navbar />
			<Header />
			<Container>
				<Row>
					<Col lg='3' className='menuLateral'>
						<Leftbar/>
					</Col>
					<Col md='12' lg='9' style={{backgroundColor: "white"}}>

						<strong> Atas - CODAINF </strong><br/>

						<Link to={require("./Arquivos/codainf/ata-002-22-nov-2013.pdf")}>
							ata-002-22-nov-2013</Link><br/>
						<Link to={require("./Arquivos/codainf/ata-001-24-out-2014.pdf")}>
							ata-001-24-out-2014</Link><br/>
						<Link to={require("./Arquivos/codainf/ata-003-28-mai-2015.pdf")}>
							ata-003-28-mai-2015</Link><br/>
						<Link to={require("./Arquivos/codainf/ata-004-02-jul-2015.pdf")}>
							ata-004-02-jul-2015</Link><br/>
						<Link to={require("./Arquivos/codainf/ata-009-01-abr-2016.pdf")}>
							ata-009-01-abr-2016</Link><br/>
						<Link to="aHR0cHM6Ly9wYXN0ZWJpbi5jb20vUkVpZ3ZkZTMK">
							ata-015-22-nov-2016</Link><br/>
						<Link to={require("./Arquivos/codainf/ata-010-12-mai-2016.pdf")}>
							ata-010-12-mai-2016</Link><br/>
						<Link to={require("./Arquivos/codainf/ata-011-16-jun-2016.pdf")}>
							ata-011-16-jun-2016</Link><br/>
						<Link to={require("./Arquivos/codainf/ata-012-04-ago-2016.pdf")}>
							ata-012-04-ago-2016</Link><br/>
						<Link to={require("./Arquivos/codainf/ata-013-04-out-2016.pdf")}>
							ata-013-04-out-2016</Link><br/>
						<Link to={require("./Arquivos/codainf/ata-014-11-out-2016.pdf")}>
							ata-014-11-out-2016</Link><br/>
						<Link to={require("./Arquivos/codainf/ata-015-22-nov-2016.pdf")}>
							ata-015-22-nov-2016</Link><br/>
						<br/>
						<br/>
					</Col>
				</Row>
			</Container>
			<Footer />
		</div>
	);
}

export default PaginaModelo;