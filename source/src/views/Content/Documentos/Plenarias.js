import React from "react"

import Navbar from "components/common/Navbar.js";
import Footer from "components/common/Footer.js";
import Leftbar from "components/common/Leftbar.js";
import Header from "components/home/Header.js";

import { Container, Row, Col } from 'reactstrap';
import {Link} from "react-router-dom"
import "assets/css/custom.css"


function PaginaModelo (props){
	return (
		<div className="edit">
			<Navbar />
			<Header />
			<Container>
				<Row>
					<Col lg='3' className='menuLateral'>
						<Leftbar/>
					</Col>
					<Col md='12' lg='9' style={{backgroundColor: "white"}}>

						<strong> Atas - Plenarias DAINF </strong><br/>

						<Link to={require("./Arquivos/plenarias-dainf/ata-001-08-nov-2012.pdf")}>
						ata-001-08-nov-2012</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata-011-22-ago-2013.pdf")}>
						ata-011-22-ago-2013</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata-020-28-nov-2014.pdf")}>
						ata-020-28-nov-2014</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata-029-05-nov-2015.pdf")}>
						ata-029-05-nov-2015</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata040-10-03-2017.pdf")}>
						ata040-10-03-2017</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata48.pdf")}>
						ata48</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata-002-18-dez-2012.pdf")}>
						ata-002-18-dez-2012</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata-012-13-set-2013.pdf")}>
						ata-012-13-set-2013</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata-021-12-dez-2014.pdf")}>
						ata-021-12-dez-2014</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata-030-03-dez-2015.pdf")}>
						ata-030-03-dez-2015</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata041-27-04-2017.pdf")}>
						ata041-27-04-2017</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata50.pdf")}>
						ata50</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata-003-26-fev-2013.pdf")}>
						ata-003-26-fev-2013</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata-013-18-out-2013.pdf")}>
						ata-013-18-out-2013</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata-022-06-fev-2015.pdf")}>
						ata-022-06-fev-2015</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata-031-18-fev-2016.pdf")}>
						ata-031-18-fev-2016</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata-042-07-06-2017.pdf")}>
						ata-042-07-06-2017</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata5123out2018.pdf")}>
						ata5123out2018</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata-004-12-mar-2013.pdf")}>
						ata-004-12-mar-2013</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata-014-25-out-2013.pdf")}>
						ata-014-25-out-2013</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata-023-03-mar-2015.pdf")}>
						ata-023-03-mar-2015</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata-032-05-abr-2016.pdf")}>
						ata-032-05-abr-2016</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata-043-20-10-2017.pdf")}>
						ata-043-20-10-2017</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata52.pdf")}>
						ata52</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata-005-02-fev-2013.pdf")}>
						ata-005-02-fev-2013</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata-015-14-fev-2014.pdf")}>
						ata-015-14-fev-2014</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata-024-07abr2015.pdf")}>
						ata-024-07abr2015</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata-033-24-jun-2016.pdf")}>
						ata-033-24-jun-2016</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata044-17-11-2017.pdf")}>
						ata044-17-11-2017</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata-006-26-abr-2013.pdf")}>
						ata-006-26-abr-2013</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata-016-10-mar-2014.pdf")}>
						ata-016-10-mar-2014</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata-025-05-mai-2015.pdf")}>
						ata-025-05-mai-2015</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata-034-04-ago-2016.pdf")}>
						ata-034-04-ago-2016</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata045-18-12-2017.pdf")}>
						ata045-18-12-2017</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata-007-28-mai-2013.pdf")}>
						ata-007-28-mai-2013</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata-017-11-abr-2014.pdf")}>
						ata-017-11-abr-2014</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata-026-02-jun-2015.pdf")}>
						ata-026-02-jun-2015</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata-035-23-ago-2016.pdf")}>
						ata-035-23-ago-2016</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata046-11-04-2018.pdf")}>
						ata046-11-04-2018</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata-008-21-jun-2013.pdf")}>
						ata-008-21-jun-2013</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata-018-12-set-2014.pdf")}>
						ata-018-12-set-2014</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata-027-06-ago-2015.pdf")}>
						ata-027-06-ago-2015</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata-036-20-out-2016.pdf")}>
						ata-036-20-out-2016</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata049-14-08-2018.pdf")}>
						ata049-14-08-2018.PDF</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata-009-05-jul-2013.pdf")}>
						ata-009-05-jul-2013</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata-019-26-set-2014.pdf")}>
						ata-019-26-set-2014</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata-028-01-out-2015.pdf")}>
						ata-028-01-out-2015</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata039-08-02-2017.pdf")}>
						ata039-08-02-2017</Link><br/>
						<Link to={require("./Arquivos/plenarias-dainf/ata47.pdf")}>
						ata47</Link><br/>
						<br/>
						<br/>
					</Col>
				</Row>
			</Container>
			<Footer />
		</div>
	);
}

export default PaginaModelo;