import React from "react"

import Navbar from "components/common/Navbar.js";
import Footer from "components/common/Footer.js";
import Leftbar from "components/common/Leftbar.js";
import Header from "components/home/Header.js";

import { Container, Row, Col } from 'reactstrap';
import {Link} from "react-router-dom"
import "assets/css/custom.css"


function Resolucoes (props){
	return (
		<div className="edit">
			<Navbar />
			<Header />
			<Container>
				<Row>
					<Col lg='3' className='menuLateral'>
						<Leftbar/>
					</Col>
					<Col md='12' lg='9' style={{backgroundColor: "white"}}>

						<strong> Resoluções </strong><br/>

						<Link to={require("./Arquivos/Resolucoes/ResAfasDout/file")}>
						Resolução Afastamento Doutorado</Link><br/>
						<Link to={require("./Arquivos/Resolucoes/ResAreas/file")}>
						Resoluções Áreas</Link><br/>
						<Link to={require("./Arquivos/Resolucoes/ResLicCap/file")}>
						Resoluções Licenças/Capacitação</Link><br/>
						<br/>
						<br/>
					</Col>
				</Row>
			</Container>
			<Footer />
		</div>
	);
}

export default Resolucoes;