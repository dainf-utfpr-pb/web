import React from "react"

import Navbar from "components/common/Navbar.js";
import Footer from "components/common/Footer.js";
import Leftbar from "components/common/Leftbar.js";
import Header from "components/home/Header.js";

import { Container, Row, Col } from 'reactstrap';
import {Link} from "react-router-dom"
import "assets/css/custom.css"


function PaginaModelo (props){
	return (
		<div className="edit">
			<Navbar />
			<Header />
			<Container>
				<Row>
					<Col lg='3' className='menuLateral'>
						<Leftbar/>
					</Col>
					<Col md='12' lg='9' className="colStyle">


						<h2><strong> Infraestrutura </strong></h2>
						<hr/>
						A infraestrutura do Departamento Acadêmico de Informática compreende: laboratórios, salas de aula, salas de professores e salas de projetos.
						<hr/>
						<Link className="hiperLink" to="/">Laboratórios<br/></Link>
						Laboratórios do Departamento Acadêmico de Informática
						<hr/>
						<Link className="hiperLink" to="/">Salas de professores, coordenações e atendimento a alunos</Link><br/>
						Salas de professores, coordenações de curso, chefia de departamento, reuniões e atendimento a alunos.
						<hr/>
						<Link className="hiperLink" to="/">Salas de Projetos</Link>
						<hr/>
					</Col>
				</Row>
			</Container>
			<Footer />
		</div>
	);
}

export default PaginaModelo;