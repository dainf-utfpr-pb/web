import React from "react"

import Navbar from "components/common/Navbar.js";
import Footer from "components/common/Footer.js";
import Header from "components/home/Header.js";

import Leftbar from "components/common/Leftbar.js";
import { Container, Row, Col } from 'reactstrap';
//import {Link} from "react-router-dom"
import "assets/css/custom.css"


function PaginaModelo (props){
	return (
		<div className="edit">
			<Navbar />
			<Header />
			<Container>
				<Row>
					<Col lg='3' className='menuLateral'>
						<Leftbar/>
					</Col>
					<Col md='12' lg='9' className="colStyle">
						<h2><strong> Graduação </strong></h2>
						<hr/>
						 O Departamento Acadêmico de Informática é responsável pelos cursos de
						 graduação em <a className="hiperLink" href="http://portal.utfpr.edu.br/cursos/graduacao/bacharelado/engenharia-da-computacao">
						Engenharia de Computação</a> &nbsp; e &nbsp;
						<a className="hiperLink" href="http://portal.utfpr.edu.br/cursos/graduacao/tecnologia/tecnologia-em-analise-e-desenvolvimento-de-sistemas">
						Tecnologia em Análise e Desenvolvimento de Sistemas</a>.
						<br/>
						<br/>
						 O curso de <strong>Tecnologia em Análise e Desenvolvimento de Sistemas</strong> prepara
						 o aluno para que possa atuar de forma profissional, empreendedora e
						 cidadã no desenvolvimento de produtos e serviços de informática, tanto
						 no mercado de trabalho quanto no empreendimento do próprio negócio.
						 Esses graduados são qualificados em análise, projeto, desenvolvimento,
						 implementação e gerência de sistemas computacionais. Anualmente são admitidos
						 88 estudantes na Engenharia de Computação.
						<br/>
						<br/>
						 O curso de <strong>Engenharia de Computação</strong> qualifica profissionais para atuar nas
						 áreas em que os conhecimentos de eletrônica e computação são essenciais e
						 complementares, tais como Sistemas Embarcados, Instrumentação e Controle,
						 Tecnologia de Informação, Redes de Comunicação e Fundamentos de  Programação.
						 Esta formação se revela inteiramente consistente com as tendências no desenvolvimento
						 de sistemas eletrônicos computacionais nos aspectos de hardware e software.
						 Anualmente são admitidos 52 novos estudantes em Tecnologia em Análise e
						 Desenvolvimento de Sistemas.
						<br/>
						<br/>
						 Além destes cursos, o DAINF é responsável por disciplinas na área computação
						 nos cursos de Engenharia Civil , Engenharia Elétrica e Engenheria Mecânica,
						 do Câmpus Pato Branco.
						<br/>
						<br/>
					</Col>
				</Row>
			</Container>
			<Footer />
		</div>
	);
}

export default PaginaModelo;