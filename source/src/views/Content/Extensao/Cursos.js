
import React from "react";

import Navbar from "components/common/Navbar.js";
import Footer from "components/common/Footer.js";
import Leftbar from "components/common/Leftbar.js";
import Header from "components/home/Header.js";

import { Container, Row, Col } from 'reactstrap';

import "assets/css/custom.css"

function Cursos (props){

	return (
		<div className="edit">
			<Navbar />
            <Header />
			<Container>
				<Row>
					<Col lg='3' className='menuLateral'>
						<Leftbar/>
					</Col>
					<Col md='12' lg='9' className="colStyle">
						<h2><strong> Cursos </strong></h2>
						<hr/>
						<strong> Cursos encerrados </strong>
						<hr/>
						<strong></strong>

						<br/>
						<hr/>
						<strong> Projetos de Extensão </strong>
						<hr/>
						<strong> Palestras </strong><br/>
						 Palestras realizadas pelo Departamento Acadêmico de Informática (DAINF).
						<br/>
						<br/>
					</Col>
				</Row>
			</Container>
			<Footer />
		</div>
	);
}

export default Cursos;