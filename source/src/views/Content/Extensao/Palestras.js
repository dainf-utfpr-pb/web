import React from "react"

import Navbar from "components/common/Navbar.js";
import Footer from "components/common/Footer.js";
import Leftbar from "components/common/Leftbar.js";
import Header from "components/home/Header.js";

import { Container, Row, Col } from 'reactstrap';
import {Link} from "react-router-dom"
import "assets/css/custom.css"


function PaginaModelo (props){
	return (
		<div className="edit">
			<Navbar />
			<Header />
			<Container>
				<Row>
					<Col lg='3' className='menuLateral'>
						<Leftbar/>
					</Col>
					<Col md='12' lg='9' className="colStyle">
						<h2><strong> Extensão </strong></h2>
						<hr/>
						<Link to="/home"><strong> Cursos de Extensão </strong></Link>
						<hr/>
						 Os cursos de extensão realizados pelo Departamento Acadêmico de Informática
						 (DAINF) tem como objetivo o enriquecimento curricular, o aperfeiçoamento e o
						 treinamento profissional da comunidade interna e externa da Universidade
						 Tecnológica Federal do Paraná – UTFPR, câmpus Pato Branco. Os cursos são,
						 na maioria dos casos, gratuítos.
						<br/>
						<hr/>
						<strong> Projetos de Extensão </strong>
						<hr/>
						<strong> Palestras </strong><br/>
						 Palestras realizadas pelo Departamento Acadêmico de Informática (DAINF).
						<br/>
						<br/>
					</Col>
				</Row>
			</Container>
			<Footer />
		</div>
	);
}

export default PaginaModelo;