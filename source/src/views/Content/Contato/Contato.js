import React from "react"

import Navbar from "components/common/Navbar.js";
import Footer from "components/common/Footer.js";
import Leftbar from "components/common/Leftbar.js";
import Header from "components/home/Header.js";

import { Container, Row, Col } from 'reactstrap';
import {Link} from "react-router-dom"
import "assets/css/custom.css"


function Contato (props){
	return (
		<div className="edit">
			<Navbar />
			<Header />
			<Container>
				<Row>
					<Col lg='3' className='menuLateral'>
						<Leftbar/>
					</Col>
					<Col md='12' lg='9' className="colStyle">
						<h2><strong> Contato </strong></h2>
						<hr/>
						<strong> Secretaria do DAINF </strong><br/>

						Local: Bloco S, sala 101<br/>
						Estagiária: Ana Claudia Leonardi (analeonardi@alunos.utfpr.edu.br)<br/>
						Fone: (46) 3220-2591<br/>
						<br/>

						<hr/>
						<strong> Chefia do Departamento de Informática - DAINF </strong><br/>

						Prof. Bruno César Ribas - (3220-2691)<br/>
						Email: dainf-pb<img alt="" src={require("assets/img/acirculo.png")} style={{width:"19px"}}/>utfpr.edu.br<br/>
						<br/>

						<hr/>
						<strong> Coordenação do Curso de Engenharia de Computação - COENC </strong><br/>

						Prof. Pablo Gautério Cavalcanti - (3220-2690)<br/>
						Email: coenc-pb<img alt="" src={require("assets/img/acirculo.png")} style={{width:"19px"}}/>utfpr.edu.br<br/>
						<br/>

						<hr/>
						<strong> Coordenação do Curso de Tecn. em Análise e Desenv. de Sistemas - COADS </strong><br/>

						Prof. Edilson Pantarolo - (3220-2590)<br/>
						Email: coads-pb<img alt="" src={require("assets/img/acirculo.png")} style={{width:"19px"}}/>utfpr.edu.br<br/>
						<br/>

						<hr/>
						<strong> Laboratórios de Informática - Bloco V </strong><br/>

						Gustavo Arcari - (3220-2592)<br/>
						Email: gustavoarcari<img alt="" src={require("assets/img/acirculo.png")} style={{width:"19px"}}/>utfpr.edu.br<br/>
						<br/>

						<Link to={require("./Salasdosprofessores.pdf")}><strong>Todos os ramais e salas</strong></Link><br/>
						<br/>

					</Col>
				</Row>
			</Container>
			<Footer />
		</div>
	);
}

export default Contato;