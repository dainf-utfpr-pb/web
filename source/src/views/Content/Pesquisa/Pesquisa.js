import React from "react"

import Navbar from "components/common/Navbar.js";
import Footer from "components/common/Footer.js";
import Leftbar from "components/common/Leftbar.js";
import Header from "components/home/Header.js";

import { Container, Row, Col } from 'reactstrap';
import {Link} from "react-router-dom"
import "assets/css/custom.css"


function PaginaModelo (props){
	return (
		<div className="edit">
			<Navbar />
			<Header />
			<Container>
				<Row>
					<Col lg='3' className='menuLateral'>
						<Leftbar/>
					</Col>
					<Col md='12' lg='9' className="colStyle">
						<h2><strong> Pesquisa </strong></h2>
						<hr/>
						<Link className="hiperLink" to="/"> Grupos de Pesquisa </Link>
						<br/>
						<br/>
						<Link className="hiperLink" to="/"> Projetos de Desenvolvimento e Inovação </Link>
						<br/>
						<br/>
						<Link className="hiperLink" to="/"> Projetos de Pesquisa </Link>
					</Col>
				</Row>
			</Container>
			<Footer />
		</div>
	);
}

export default PaginaModelo;