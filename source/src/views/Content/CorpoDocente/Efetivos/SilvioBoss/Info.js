export const Nome = "Silvio Luiz Bragatto Boss"
export const Atribuicoes = "Professor do Magistério Superior"
export const EmailInstitucional = "silvioboss@utfpr.edu.br"
export const EmailAlternativo = ""
export const Ramal = "(46) 3220-2593"
export const Sala = "105 - Bloco S"
export const Lattes = "http://lattes.cnpq.br/5805926452305147"
export const Pagina = ""
export const Regime = "Dedicação Exclusiva"
export const Ingresso = "Julho/2011"
export const Formacao  = [
"Mestrado em Informática (UFPR, Curitiba-PR, 2010)",
"Graduação em Análise de Sistemas (UNICENTRO, Guarapuava-PR, 2006)"
]
export const Interesses = [
"Teoria da computação",
"Teoria dos grafos",
"Algoritmos",
"Algoritmos de busca em grafos, multigrafos e hipergrafos",
"Linguagens de programação"
]