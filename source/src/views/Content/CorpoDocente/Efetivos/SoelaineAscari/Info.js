export const Nome = "Soelaine Rodrigues Ascari"
export const Atribuicoes = "Professor do Magistério Superior"
export const EmailInstitucional = "soelaine@utfpr.edu.br"
export const EmailAlternativo = ""
export const Ramal = "(46) 3220-2693"
export const Sala = "103 - Bloco S"
export const Lattes = "http://lattes.cnpq.br/7145298480843878"
export const Pagina = ""
export const Regime = "Dedicação Exclusiva"
export const Ingresso = "Fevereiro/2004"
export const Formacao = [
"Mestrado em Ciências da Computação (UFSC, Florianópolis-SC, 2003)",
"Graduação em Tecnologia em Processamento de Dados (UNIVEL, Cascavel-PR, 1999)"
]
export const Interesses = [
"Interação Ser Humano Computador",
"Arquitetura de computadores",
"Análise e projeto de sistemas",
"Engenharia de Software",
]