export const Nome = "Géri Natalino Dutra"
export const Atribuicoes = "Professor do Magistério Superior"
export const EmailInstitucional = "natalino@utfpr.edu.br"
export const EmailAlternativo = "geridutraarroba.pnghotmail.com"
export const Ramal = "(46) 3224-6138"
export const Sala = ""
export const Lattes = "http://lattes.cnpq.br/9921945692078074"
export const Pagina = ""
export const Regime = "Cedido para Prefeitura"
export const Ingresso = "Junho/1996"
export const Formacao = [
"Mestrado em Informática (UFPR, Curitiba-PR, 2004)",
"Graduação em Tecnologia em Processamento de Dados (CEFET, Pato Branco-PR, 1994)"
];

export const Interesses = [
"Engenharia de Software",
"Desenvolvimento de Software",
"Análise e desenvolvimento de sistemas",
"Linguagens de programação",
"Algoritmos e Estrutura de Dados"
];