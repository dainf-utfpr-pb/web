export const Nome = "Edilson Pontarolo"
export const Atribuicoes = "Professor do Ensino Básico, Técnico e Tecnológico"
export const EmailInstitucional = "epontarolo@utfpr.edu.br"
export const EmailAlternativo = ""
export const Ramal = "(46) 3220-2692"
export const Sala = "Coordenação do curso de Tecnologia em Análise e Desenvolvimento de Sistemas - Bloco S"
export const Lattes = "http://lattes.cnpq.br/0637811527251299"
export const Pagina = "http://paginapessoal.utfpr.edu.br/epontarolo"
export const Regime = "Dedicação Exclusiva"
export const Ingresso = "Maio/1997"
export const Formacao = [
"Doutorado em Informática na Educação (UFRGS, Porto Alegre-RS, 2008)",
"Mestrado em Engenharia Elétrica e Informática Industrial (UTFPR, Curitiba-PR, 1998)",
"Bacharel em Informática (UEPG, Ponta Grossa-PR, 1995)"
];

export const Interesses = [
"Modelagem afetivo-cognitiva do aluno",
"Sistemas inteligentes de ensino aprendizagem",
"Aprendizagem colaborativa apoiada por computador",
"Jogos digitais educacionais",
"Objetos de aprendizagem",
"Representações metacognitivas e aprendizagem"
];