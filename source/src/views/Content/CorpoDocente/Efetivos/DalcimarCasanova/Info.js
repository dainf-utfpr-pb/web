export const Nome = "Dalcimar Casanova"
export const Atribuicoes = "Professor do Magistério Superior"
export const EmailInstitucional = "casanova@utfpr.edu.br"
export const EmailAlternativo = "dalcimar@gmail.com"
export const Ramal = "(46) 3220-2591 / (46) 3220-2693"
export const Sala = "102 - Bloco S"
export const Lattes = "http://lattes.cnpq.br/4155115530052195"
export const Pagina = "http://sites.google.com/site/dalcimar"
export const Regime = "Dedicação Exclusiva"
export const Ingresso = "Fevereiro/2015"
export const Formacao = [
"Doutorado em Física (USP, São Paulo-SP, 2013)",
"Mestrado em Ciências da Computação e Matemática Computacional (USP, São Paulo-SP, 2008)",
"Graduação em em Ciência da Computação (UNOESC, Chapecó-SC, 2005)"
];
export const Interesses = [
"Visão Computacional",
"Processamento de Imagens",
"Aprendizado de Máquina",
"Sistemas Inteligentes",
"Sistemas Multiagentes"
];