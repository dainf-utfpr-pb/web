export const Nome = "Eden Ricardo Dosciatti"
export const Atribuicoes = "Professor do Ensino Básico, Técnico e Tecnológico"
export const EmailInstitucional = "edenrd@utfpr.edu.br"
export const EmailAlternativo = ""
export const Ramal = "(46) 3220-2591"
export const Sala = "Chefia DAINF - Bloco S"
export const Lattes = "http://lattes.cnpq.br/7673919686043920"
export const Pagina = "http://pessoal.utfpr.edu.br/edenrd"
export const Regime = "Dedicação Exclusiva"
export const Ingresso = "Maio/1997"
export const Formacao = [
"Doutorado em Engenharia Elétrica e Informática Industrial (UTFPR, Curitiba-PR, 2015)",
"Mestrado em Engenharia Elétrica e Informática Industrial (UTFPR, Curitiba-PR, 2010)",
"Especialização em Gestão da Informática (FAE, Curitiba-PR, 1997)",
"Graduação em Análise de Sistemas (UNISINOS, Porto Alegre-RS, 1991)"
];
export const Interesses = [
"Teleinformática",
"Algoritmos de escalonamento",
"QoS",
"Sistemas de computação",
"Redes de computadores"
];