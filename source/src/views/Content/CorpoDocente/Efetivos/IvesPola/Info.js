export const Nome = "Ives Rene Venturini Pola"
export const Atribuicoes = "Professor do Magistério Superior - Adjunto A"
export const EmailInstitucional = "ivesr@utfpr.edu.br"
export const EmailAlternativo = ""
export const Ramal = "(46) 3220-2693"
export const Sala = "Sala de Professores - Bloco S103"
export const Lattes = "http://lattes.cnpq.br/9238482905472474"
export const Pagina = ""
export const Regime = "Dedicação Exclusiva"
export const Ingresso = ""
export var Formacao = [
"Doutorado em Ciências da Computação e Matemática Computacional (USP, São Paulo, 2010)",
"Mestrado em Ciências da Computação e Matemática Computacional (USP, São Paulo, 2005)",
"Bacharel em Ciência da Computação (UNESP, São Paulo, 2003)"
]
export const Interesses = [
"Banco de Dados",
"Linguagens de Programação",
"Sistemas de Informação",
"Sofware básico"
]