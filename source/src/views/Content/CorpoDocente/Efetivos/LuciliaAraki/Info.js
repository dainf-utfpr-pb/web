export const Nome = "Lucilia Yoshie Araki"
export const Atribuicoes = "Professor do Magistério Superior"
export const EmailInstitucional = "araki@utfpr.edu.br"
export const EmailAlternativo = ""
export const Ramal = "(46) 3220-2691"
export const Sala = "108 - Bloco S"
export const Lattes = "http://lattes.cnpq.br/7282749668662090"
export const Pagina = ""
export const Regime = "Dedicação Exclusiva"
export const Ingresso = "Fevereiro/2013"
export const Formacao = [
"Mestrado em Informática (UFPR, Curitiba-PR, 2009)",
"Graduação em Informática (UNIOESTE, Cascavel-PR, 2006)"
]
export const Interesses = [
"Teste de Software",
"Engenharia de Software",
"Qualidade de Software",
"Usabilidade"
]