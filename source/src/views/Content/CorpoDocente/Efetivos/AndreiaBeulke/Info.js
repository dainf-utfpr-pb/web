export const Nome = "Andreia Scariot Beulke "
export const Atribuicoes = "Professor do Ensino Básico, Técnico e Tecnológico"
export const EmailInstitucional = "andreiabeulke@utfpr.edu.br"
export const EmailAlternativo = "andreia.scariot.b@gmail.com"
export const Ramal = "(46) 3220-2694"
export const Sala = "107 - Bloco S"
export const Lattes = "http://lattes.cnpq.br/5104681948982773"
export const Pagina = ""
export const Regime = "Dedicação Exclusiva"
export const Ingresso = "Fevereiro/2013"
export const Formacao = [
"Especialização em Tecnologia Java (UTFPR, Pato Branco-PR, 2009)",
"Graduação em Tecnologia em Informática (CEFET-PR, Pato Branco-PR, 2004)"
]
export const Interesses = [
"Sistemas Multimídia",
"Sistemas Cliente/Servidor",
"Sistemas de Informação para Web",
"Linguagens de Programação (Java e PHP)",
"Engenharia de Software"
]
