export const Nome = "Luis Cassiano Goularte Rista"
export const Atribuicoes = "Professor do Magistério Superior"
export const EmailInstitucional = "rista@utfpr.edu.br"
export const EmailAlternativo = "lcg.rista@gmail.com"
export const Ramal = "(46) 3220-2591"
export const Sala = ""
export const Lattes = "http://lattes.cnpq.br/4778030265275310"
export const Pagina = "http://pessoal.utfpr.edu.br/rista"
export const Regime = "Dedicação Exclusiva"
export const Ingresso = "Junho/2012"
export const Formacao = [
"Mestrado em Ciência da Computação (UFSC, Florianópolis-SC, 2005)",
"Graduação em Ciência da Computação (UNICRUZ, Cruz Alta-RS, 2000)"
]
export const Interesses = [
"Processamento Paralelo e Distribuído",
"Sistemas Distribuídos de Alto Desempenho",
"Avaliação de Desempenho de Sistemas Computacionais",
"Redes de Computadores",
"Computação Móvel"
]