export const Nome = "Luis Carlos Ferreira Bueno"
export const Atribuicoes = "Professor do Magistério Superior"
export const EmailInstitucional = "lcfb@utfpr.edu.br"
export const EmailAlternativo = ""
export const Ramal = "(46) 3220-2591"
export const Sala = ""
export const Lattes = "http://lattes.cnpq.br/4243551773071823"
export const Pagina = ""
export const Regime = "Dedicação Exclusiva"
export const Ingresso = "Fevereiro/1994"
export const Formacao = [
"Mestrado em Informática (UFPR, Curitiba-PR, 2008)",
"Especialização em Informática (UFPR, Curitiba-PR, 1992)",
"Graduação em Tecnologia em Processamento de Dados (FUNESP, Pato Branco-PR, 1989)"
]
export const Interesses = [
"Programação .NET",
"Banco de Dados",
"Inteligência Artificial",
"Programação para Dispositivos Móveis",
"Xadrez"
]