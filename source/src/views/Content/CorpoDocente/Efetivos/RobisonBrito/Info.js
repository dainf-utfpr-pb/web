export const Nome = "Robison Cris Brito"
export const Atribuicoes = "Professor do Ensino Básico, Técnico e Tecnológico"
export const EmailInstitucional = "robison@utfpr.edu.br"
export const EmailAlternativo = ""
export const Ramal = "(46) 3220-2693"
export const Sala = "103 - Bloco S"
export const Lattes = "http://lattes.cnpq.br/2797864104656777"
export const Pagina = ""
export const Regime = "Dedicação Exclusiva"
export const Ingresso = "Fevereiro/2004"
export const Formacao = [
"Mestrado em Engenharia Elétrica e Informática Industrial (UTFPR, Curitiba-PR, 2009)",
"Especialização em Desenvovimento para Ambiente Internet (CEFET-PR, Pato Branco, 2003)",
"Graduação em Tecnologia em Informática (CEFET-PR, Pato Branco-PR, 2001)"
]
export const Interesses = [
"Computação móvel",
"Novas tecnologias: rfid, javacard e computação pervasiva",
"Desenvolvimento web ou desktop com Java"
]