export const Nome = "Richardson Ribeiro"
export const Atribuicoes = "Professor do Magistério Superior"
export const EmailInstitucional = "richardsonr@utfpr.edu.br"
export const EmailAlternativo = ""
export const Ramal = "(46) 3220-2591"
export const Sala = ""
export const Lattes = "http://lattes.cnpq.br/1732432214014006"
export const Pagina = ""
export const Regime = "Dedicação Exclusiva"
export const Ingresso = "Novembro/2010"
export const Formacao = [
"Doutorado em Informática (PUC-PR, Curitiba-PR, 2010)",
"Mestrado em Informática (PUC-PR, Curitiba-PR, 2006)",
"Graduação em Bacharelado em Informática (UNC, Mafra-SC, 2003)"
]
export const Interesses = [
"Inteligência artificial",
"Métodos inteligentes de otimização",
"Sistemas multiagentes",
"Aprendizagem de máquina",
"Coordenação reativa"
]