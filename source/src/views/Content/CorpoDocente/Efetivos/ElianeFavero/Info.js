export const Nome = "Eliane Maria De Bortoli Fávero"
export const Atribuicoes = "Professor do Magistério Superior"
export const EmailInstitucional = "elianedb@utfpr.edu.br"
export const EmailAlternativo = ""
export const Ramal = "(46) 3220-2694"
export const Sala = "107 - Bloco S"
export const Lattes = "http://lattes.cnpq.br/2623789586746722"
export const Pagina = ""
export const Regime = "Dedicação Exclusiva"
export const Ingresso = "Novembro/2009"
export const Formacao = [
"Mestrado em Engenharia Elétrica e Informática Industrial (UTFPR, Curitiba-PR, 2006)",
"Especialização em Ciência da Computação (UFSC, Florianópolis-SC, 2003)",
"Graduação em Tecnologia em Processamento de Dados (CEFET, Pato Branco-PR, 1999)"
];
export const Interesses = [
    "Engenharia de software",
    "Tecnologias educacionais",
    "Redes sociais, comunidades virtuais e sistemas colaborativos"
];
