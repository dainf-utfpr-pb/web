export const Nome = "Pablo Gautério Cavalcanti"
export const Atribuicoes = "Professor do Magistério Superior"
export const EmailInstitucional = "pgcavalcanti@utfpr.edu.br"
export const EmailAlternativo = ""
export const Ramal = "(46) 3220-2693"
export const Sala = "102 - Bloco S"
export const Lattes = "http://lattes.cnpq.br/7803338696833069"
export const Pagina = ""
export const Regime = "Dedicação Exclusiva"
export const Ingresso = "Fevereiro/2015"
export const Formacao = [
"Doutorado em Computação(UFRGS, Porto Alegre-RS, 2013)",
"Graduação em Engenharia de Computação (FURG, Rio Grande-RS, 2007)"
]
export const Interesses = [
"Processamento de dados",
"Visão computacional",
"Reconhecimento de padrões"
]