export const Nome = "Rúbia Eliza de Oliveira Schultz Ascari"
export const Atribuicoes = "Professor do Magistério Superior"
export const EmailInstitucional = "rubia@utfpr.edu.br"
export const EmailAlternativo = "rubiaschultz@gmail.com"
export const Ramal = "(46) 3220-2694"
export const Sala = "107 - Bloco S"
export const Lattes = "http://lattes.cnpq.br/8086913547704538"
export const Pagina = ""
export const Regime = "Dedicação Exclusiva"
export const Ingresso = "Outubro/2009"
export const Formacao = [
"Mestrado em Engenharia Elétrica e Informática Industrial (UTFPR, Curitiba-PR, 2007).",
"Especialização em Desenvolvimento Para Ambiente Internet (CEFET, Pato Branco-PR, 2003)",
"Graduação em Tecnologia em Informática (CEFET, Pato Branco-PR, 2001)"
]
export const Interesses = [
"Programação",
"Padrões de Projeto",
"Inteligência Artificial (Lógica Fuzzy, Algoritmos Genéticos, Programação Genética)",
"Engenharia de software",
"Análise e desenvolvimento de software",
"Processamento digital de imagens",
"Biomédica"
]