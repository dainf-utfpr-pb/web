export const Nome = "Kathya Silvia Collazos Linares"
export const Atribuicoes = "Professor do Magistério Superior"
export const EmailInstitucional = "kathya@utfpr.edu.br"
export const EmailAlternativo = ""
export const Ramal = "(46) 3220-2691"
export const Sala = "108 - Bloco S"
export const Lattes = "http://lattes.cnpq.br/5617559158119618"
export const Pagina = "http://pessoal.utfpr.edu.br/kathya"
export const Regime = "Dedicação Exclusiva"
export const Ingresso = "Fevereiro/2010"
export const Formacao = [
"Doutorado em Engenharia Elétrica (UFSC, Florianópolis-SC, 2003)",
"Mestrado em Engenharia Elétrica (UFSC, Florianópolis-SC, 1997)",
"Graduação em Ingeniería Electrónica (UNMSM, Lima-Peru, 1992)"
]
export const Interesses = [
"Aplicações de inteligência artificial",
"Informática na saúde",
"Datamining e análise de dados"
]