
import React from "react";

import Navbar from "components/common/Navbar.js";
import Footer from "components/common/Footer.js";
import Leftbar from "components/common/Leftbar.js";
import Header from "components/home/Header.js";

import { Container, Row, Col } from 'reactstrap';

import LayoutProfessor from "components/common/LayoutProfessor.js"

import { Nome, 
Atribuicoes,
EmailInstitucional,
EmailAlternativo,
Ramal, 
Sala ,
Lattes, 
Pagina,
Regime,
Ingresso,
Formacao,
Interesses, } from "views/Content/CorpoDocente/Efetivos/KathyaLinares/Info.js"

import "assets/css/custom.css"

function KathyaLinares() {

	return (
		<div className="edit">
			<Navbar />
            <Header />
			<Container>
				<Row>
					<Col lg='3' className='menuLateral'>
						<Leftbar/>
					</Col>
					<Col md='12' lg='9' style={{backgroundColor: "white"}}>
                        <LayoutProfessor
                            Nome={Nome}
                            Imagem={require("views/Content/CorpoDocente/Efetivos/KathyaLinares/Kathya.jpg")}
                            Atribuicoes={Atribuicoes}
                            EmailInstitucional={EmailInstitucional}
                            EmailAlternativo={EmailAlternativo}
                            Ramal={Ramal}
                            Sala={Sala} 
                            Lattes={Lattes} 
                            Pagina={Pagina}
                            Regime={Regime}
                            Ingresso={Ingresso}
                            Formacao={Formacao}
                            Interesses={Interesses}
                        />
					</Col>
				</Row>
			</Container>
			<Footer />
		</div>
    );   
}

export default KathyaLinares;