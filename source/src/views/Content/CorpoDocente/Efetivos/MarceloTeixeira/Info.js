export const Nome = "Marcelo Teixeira"
export const Atribuicoes = "Professor do Magistério Superior"
export const EmailInstitucional = "marceloteixeira@utfpr.edu.br"
export const EmailAlternativo = "marcelosoledade@gmail.com"
export const Ramal = "(46) 3220-2593"
export const Sala = "104 - Bloco S"
export const Lattes = "http://lattes.cnpq.br/8925349327322997"
export const Pagina = "http://paginapessoal.utfpr.edu.br/marceloteixeira"
export const Regime = "Dedicação Exclusiva"
export const Ingresso = "Agosto/2013"
export const Formacao = [
"Doutorado em Engenharia de Automação e Sistemas (UFSC, Florianópolis-SC, 2013)",
"Mestrado em Engenharia de Computação (UPE, Recife-PE, 2009)",
"Graduação em Ciência da Computação - UPF (Passo Fundo-RS, 2007)"
]
export const Interesses = [
"Sistemas a eventos Discretos (SED)",
"Verificação Formal e Controle de SED",
"Ferramentas para a modelagem, a simulação e a síntese de controladores para SEDs",
"Web Services (WS), Database-as-a-Service (DBaaS), Service-Oriented Architectures (SOA)",
"Síntese automática de software"
]