export const Nome = "Vinícius Pegorini"
export const Atribuicoes = "Professor do Ensino Básico, Técnico e Tecnológico"
export const EmailInstitucional = "vinicius@utfpr.edu.br"
export const EmailAlternativo = "viniciuspegorini@gmail.com"
export const Ramal = "(46) 3220-2693"
export const Sala = "103 - Bloco S"
export const Lattes = "http://lattes.cnpq.br/9584015577266322"
export const Pagina = ""
export const Regime = "Dedicação Exclusiva"
export const Ingresso = "Outubro/2012"
export var Formacao = [
"Mestrado em Engenharia Elétrica (UTFPR, Pato Branco-PR, EM ANDAMENTO)",
"Especialização em em Tecnologia em Desenvolvimento Web - Java e .Net. (UTFPR, Pato Branco-PR, 2007)",
"Graduação em Sistemas de Informação (CEFET, Pato Branco-PR, 2005)"
]
export const Interesses = [
"Desenvolvimento Web (Java, .NET, PHP)",
"Sistemas Distribuídos",
"Programação para Dispositivos Móveis",
"Banco de Dados"
]