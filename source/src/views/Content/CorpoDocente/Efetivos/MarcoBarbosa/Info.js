export const Nome = "Marco Antônio de Castro Barbosa"
export const Atribuicoes = "Professor do Magistério Superior"
export const EmailInstitucional = "mbarbosa@utfpr.edu.br"
export const EmailAlternativo = ""
export const Ramal = "(46) 3220-2690"
export const Sala = "Coordenação de Engenharia da Computação - Bloco S"
export const Lattes = "http://lattes.cnpq.br/5794615482009389"
export const Pagina = ""
export const Regime = "Dedicação Exclusiva"
export const Ingresso = "Fevereiro/2011"
export const Formacao = [
"Doutorado em Informática (UMINHO, Braga-Portugal, 2009)",
"Mestrado em Computação (UFRGS, Porto Alegre-RS, 2001)",
"Graduação em Informática (UNICRUZ, Cruz Alta-RS, 1998)"
]
export const Interesses = [
"Fundamentos de computação",
"Complexidade de algoritmos",
"Métodos formais"
]