export const Nome = "Adriano Serckumecka"
export const Atribuicoes = "Professor do Magistério Superior"
export const EmailInstitucional = "serckumecka@utfpr.edu.br"
export const EmailAlternativo = "serckumecka@utfpr.edu.br"
export const Ramal = "(46) 3220-2693"
export const Sala = "102 - Bloco S"
export const Lattes = "http://lattes.cnpq.br/4313094198245410"
export const Pagina = ""
export const Regime = "Dedicação Exclusiva"
export const Ingresso = "Fevereiro/2013"
export const Formacao = [
"Mestrado em Computação Aplicada (UEPG, Ponta Grossa-PR, 2012)",
"Graduação em Tecnologia em Análise e Desenvolvimento de Sistema. (UTFPR, Ponta Grossa-PR, 2010)"
]
export const Interesses = [
"Sistemas Distribuídos",
"Computação Paralela",
"Grades Computacionais",
"Redes de Computadores",
"Segurança Computacional"
]