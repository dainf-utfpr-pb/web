export const Nome = "Fábio Favarim"
export const Atribuicoes = "Professor do Magistério Superior"
export const EmailInstitucional = "favarim@utfpr.edu.br"
export const EmailAlternativo = ""
export const Ramal = "(46) 3220-2555"
export const Sala = "Bloco B - DIRGRAD"
export const Lattes = "http://lattes.cnpq.br/4127225140175485"
export const Pagina = "http://paginapessoal.utfpr.edu.br/favarim"
export const Regime = "Dedicação Exclusiva"
export const Ingresso = "Março/2009"
export const Formacao = [
"Doutorado em Engenharia Elétrica (UFSC, Florianópolis-SC, 2009)",
"Mestrado em Engenharia Elétrica (UFSC, Florianópolis-SC, 2003)",
"Graduação em Informática (UNIOESTE, Cascavel-PR, 2000)"
];
export const Interesses = [
"Sistemas Distribuídos",
"Sistemas Paralelos",
"Redes de Computadores",
"Computação Móvel",
"Robótica Móvel"
];