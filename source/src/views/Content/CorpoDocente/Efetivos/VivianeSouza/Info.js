export const Nome = "Viviane Dal Molin de Souza"
export const Atribuicoes = "Professora do Magistério Superior"
export const EmailInstitucional = "vivianesouza@utfpr.edu.br"
export const EmailAlternativo = ""
export const Ramal = "(46) 3220-2591"
export const Sala = ""
export const Lattes = "http://lattes.cnpq.br/7388727951123681"
export const Pagina = ""
export const Regime = "Dedicação Exclusiva"
export const Ingresso = "Fevereiro/2014"
export var Formacao = [
"Doutorando em Informática (PUC-PR, Curitiba-PR, EM ANDAMENTO)",
"Mestrado em Engenharia de Produção e Sistemas (PUC-PR, Curitiba-PR, 2008)",
"Graduação em Ciência da Computação (UTP, Curitiba-PR, 2004)"
]
export const Interesses = [
"Mineração de Dados",
"Agentes de Software",
"Sistemas Especialistas",
"Sistemas Inteligentes",
"Otimização Multi-objetivo",
]