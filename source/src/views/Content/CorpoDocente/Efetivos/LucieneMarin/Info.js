export const Nome = "Luciene de Olivera Marin"
export const Atribuicoes = "Professor do Magistério Superior"
export const EmailInstitucional = "lucienemarin@utfpr.edu.br"
export const EmailAlternativo = "mluciene@gmail.com"
export const Ramal = "(46) 3220-2593"
export const Sala = "105 - Bloco S"
export const Lattes = "http://lattes.cnpq.br/6886938395028085"
export const Pagina = ""
export const Regime = "Dedicação Exclusiva"
export const Ingresso = "Novembro/2012"
export const Formacao = [
"Doutorado em Engenharia Elétrica (UFSC, Florianópolis-SC, 2009)",
"Mestrado em Ciências da Computação (UFSC, Florianópolis-SC, 2003)",
"Graduação em Ciência da Computação (UFMS, Campo Grande-MS, 1996)"
]
export const Interesses = [
"Inteligência Artificial",
"Redes Neurais Artificiais",
"Robótica Inteligente",
"Reconhecimento de Padrões"
]