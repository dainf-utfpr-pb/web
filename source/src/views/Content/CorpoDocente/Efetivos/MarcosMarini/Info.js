export const Nome = "Marcos Junior Marini"
export const Atribuicoes = "Professor do Ensino Básico, Técnico e Tecnológico"
export const EmailInstitucional = "marini@utfpr.edu.br"
export const EmailAlternativo = ""
export const Ramal = "(46) 3220-2541"
export const Sala = "111 - Bloco V - Laboratório de Estudos Regionais - PPGDR"
export const Lattes = "http://lattes.cnpq.br/7779355795284015"
export const Pagina = ""
export const Regime = "Dedicação Exclusiva"
export const Ingresso = "Fevereiro/1994"
export const Formacao = [
"Doutorado em Tecnologia (UTFPR, Curitiba-PR, 2012)",
"Mestrado em Informática (UFPR, Curitiba-PR, 2001)",
"Especialização em Informatica Educativa (CEFET, Curitiba, 1998)",
"Especialização em Metodologia do Ensino Tecnologico (CEFET, Pato Branco, 1995)",
"Graduação em Processamento de Dados (CEFET, Pato Branco, 1992)"
]
export const Interesses = [
"Computação Afetiva",
"Sentiment Analysis",
"Mineração de Textos",
"Aprendizagem de Máquina",
"Interface Homem-Máquina",
"Linguística Computacional"
]