export const Nome = "Mariza Miola Dosciatti"
export const Atribuicoes = "Professor do Ensino Básico, Técnico e Tecnológico"
export const EmailInstitucional = "mariza@utfpr.edu.br"
export const EmailAlternativo = "marizamioladosciatti@gmail.com"
export const Ramal = "(46) 3220-2693"
export const Sala = "Bloco S"
export const Lattes = "http://lattes.cnpq.br/2263946727235886"
export const Pagina = "www.ppgia.pucpr.br/~mariza.dosciatti"
export const Regime = "Dedicação Exclusiva"
export const Ingresso = "Agosto/1998"
export const Formacao = [
"Doutorado em Informática (PUC-PR, Curitiba-PR, EM ANDAMENTO)",
"Mestrado em Informática (UFPR, Curitiba-PR, 2001)",
"Especialização em Gestão da Informática (FAE, Curitiba-PR, 1997)",
"Graduação em Tecnologia em Processamento de Dados (CEFET, Pato Branco-PR, 1996)"
]
export const Interesses = [
"Computação Afetiva",
"Sentiment Analysis",
"Mineração de Textos",
"Aprendizagem de Máquina",
"Interface Homem-Máquina",
"Linguística Computacional"
]