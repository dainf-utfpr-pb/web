
import React from "react";
import Navbar from "components/common/Navbar.js";
import Footer from "components/common/Footer.js";
import Leftbar from "components/common/Leftbar.js";
import Linha from "components/common/Linha.js";
import Header from "components/home/Header.js";

import { Container, Row, Col } from 'reactstrap';
import "assets/css/custom.css";
import "assets/css/tabelaCorpoDocente.css";

/*Mude o nome da function de acordo com o nome da pagina*/
function CorpoDocente(props) {
	return (
		<div className="edit">
			<Navbar />
            <Header />
			<Container>
				<Row>
					<Col lg='3' className='menuLateral'>
						<Leftbar/>
					</Col>
					<Col md='12' lg='9' className="colStyle">

                        <h2><strong>Corpo Docente do Departamento Acadêmico de Informática</strong></h2>
						<hr/>
						<p className="colStyle">Está página apresenta o corpo docente do Departamento Acadêmico de Informática. Os docentes atuam principalmente nos dois cursos sob a responsabilidade da área de informática: Engenharia de Computação e Tecnologia em Análise e Desenvolvimento de Sistemas. Além destes cursos, o Departamento Acadêmico de Informática também é responsável pelas disciplinas da área de informática dos demais cursos da instituição.</p>

						<div className="janelaTabela">
							<div className="tituloUmaColuna"> <strong>Docentes efetivos</strong> </div>	

							<div style={{width: "820px"}}>
								<div className="nome" style={{backgroundColor: "#e9e9e9"}}> <strong>Nome</strong> </div>	
								<div className="titulacao" style={{backgroundColor: "#e9e9e9"}}> <strong>Titulacao</strong> </div>	
								<div className="regimedeTrabalho" style={{backgroundColor: "#e9e9e9"}}> <strong>Regime de Trabalho</strong> </div>	
								<div className="email" style={{backgroundColor: "#e9e9e9"}}> <strong>Email  @utfpr.edu.br</strong> </div>		
								<div className="lattes" style={{backgroundColor: "#e9e9e9"}}> <strong>Lattes</strong> </div>
							</div>


							<Linha
								nome="Andreia Scariot Beulke"
								link="/corpodocente/andreiaBeulke"
								titulacao="Mestre"
								regimedeTrabalho="DE"
								email="andreiabeulke"
								lattesLink="http://lattes.cnpq.br/5104681948982773"
							/>	

							<Linha
								nome="Dalcimar Casanova"
								link="/corpodocente/dalcimarCasanova"
								titulacao="Doutor"
								regimedeTrabalho="DE"
								email="casanova"
								lattesLink="http://lattes.cnpq.br/4155115530052195"
							/>

							<Linha
								nome="Eden Ricardo Dosciatti"
								link="/corpodocente/edenDosciatti"
								titulacao="Doutor"
								regimedeTrabalho="DE"
								email="edenrd"
								lattesLink="http://lattes.cnpq.br/7673919686043920"
							/>

							<Linha
								nome="Edilson Pontarolo"
								link="/corpodocente/edilsonPontarolo"
								titulacao="Doutor"
								regimedeTrabalho="DE"
								email="epontarolo"
								lattesLink="http://lattes.cnpq.br/0637811527251299"
							/>

							<Linha
								nome="Fábio Favarim"
								link="/corpodocente/fabioFavarim"
								titulacao="Doutor"
								regimedeTrabalho="DE"
								email="favarim"
								lattesLink="http://lattes.cnpq.br/4127225140175485"
							/>

							<Linha
								nome="Ives Rene Venturini Pola"
								link="/corpodocente/ivesPola"
								titulacao="Doutor"
								regimedeTrabalho="DE"
								email="ivesr"
								lattesLink="http://lattes.cnpq.br/9238482905472474"
							/>

							<Linha
								nome="Kathya Silvia Collazos Linares"
								link="/corpodocente/kathyaLinares"
								titulacao="Doutora"
								regimedeTrabalho="DE"
								email="kathya"
								lattesLink="http://lattes.cnpq.br/5617559158119618"
							/>

							<Linha
								nome="Luciene de Olivera Marin"
								link="/corpodocente/lucieneMarin"
								titulacao="Doutora"
								regimedeTrabalho="DE"
								email="lucienemarin"
								lattesLink="http://lattes.cnpq.br/6886938395028085"
							/>

							<Linha
								nome="Luis Carlos Ferreira Bueno"
								link="/corpodocente/luisBueno"
								titulacao="Doutor"
								regimedeTrabalho="DE"
								email="lcfb"
								lattesLink="http://lattes.cnpq.br/4243551773071823"
							/>

							<Linha
								nome="Marcelo Teixeira"
								link="/corpodocente/marceloTeixeira"
								titulacao="Doutor"
								regimedeTrabalho="DE"
								email="marceloteixeira"
								lattesLink="http://lattes.cnpq.br/8925349327322997"
							/>

							<Linha
								nome="Marco Antônio de Castro Barbosa"
								link="/corpodocente/marcoBarbosa"
								titulacao="Doutor"
								regimedeTrabalho="DE"
								email="mbarbosa"
								lattesLink="http://lattes.cnpq.br/5794615482009389"
							/>

							<Linha
								nome="Marcos Junior Marini"
								link="/corpodocente/marcosMarini"
								titulacao="Doutor"
								regimedeTrabalho="DE"
								email="marini"
								lattesLink="http://lattes.cnpq.br/7779355795284015"
							/>

							<Linha
								nome="Mariza Miola Dosciatti"
								link="/corpodocente/marizaDosciatti"
								titulacao="Doutora"
								regimedeTrabalho="DE"
								email="mariza"
								lattesLink="http://lattes.cnpq.br/2263946727235886"
							/>

							<Linha
								nome="Pablo Gautério Cavalcanti"
								link="/corpodocente/pabloCavalcanti"
								titulacao="Doutor"
								regimedeTrabalho="DE"
								email="pgcavalcanti"
								lattesLink="http://lattes.cnpq.br/7803338696833069"
							/>

							<Linha
								nome="Robison Cris Brito"
								link="/corpodocente/robisonBrito"
								titulacao="Mestre"
								regimedeTrabalho="DE"
								email="robison"
								lattesLink="http://lattes.cnpq.br/2797864104656777"
							/>

							<Linha
								nome="Vinícius Pegorini"
								link="/corpodocente/viniciusPegorini"
								titulacao="Mestre"
								regimedeTrabalho="DE"
								email="vinicius"
								lattesLink="http://lattes.cnpq.br/9584015577266322"
							/>

							<Linha
								nome="Viviane Dal Molin de Souza"
								link="/corpodocente/vivianeSouza"
								titulacao="Mestre"
								regimedeTrabalho="DE"
								email="vivianesouza"
								lattesLink="http://lattes.cnpq.br/7388727951123681"
							/>

							<Linha
								nome="Adriano Serckumecka"
								link="/corpodocente/adrianoSerckumecka"
								titulacao="Mestre"
								regimedeTrabalho="DE"
								email="serckumecka"
								lattesLink="http://lattes.cnpq.br/4313094198245410"
							/>

							<Linha
								nome="Eliane Maria De Bortoli Fávero"
								link="/corpodocente/elianeFavero"
								titulacao="Mestre"
								regimedeTrabalho="DE"
								email="elianedb"
								lattesLink="http://lattes.cnpq.br/2623789586746722"
							/>

							<Linha
								nome="Lucilia Yoshie Araki"
								link="/corpodocente/luciliaAraki"
								titulacao="Mestre"
								regimedeTrabalho="DE"
								email="araki"
								lattesLink="http://lattes.cnpq.br/7282749668662090"
							/>

							<Linha
								nome="Luís Cassiano Goularte Rista"
								link="/corpodocente/luisRista"
								titulacao="Mestre"
								regimedeTrabalho="DE"
								email="rista"
								lattesLink="http://lattes.cnpq.br/4778030265275310"
							/>

							<Linha
								nome="Rubia Eliza de Oliveira Schultz Ascari"
								link="/corpodocente/rubiaAscari"
								titulacao="Mestre"
								regimedeTrabalho="DE"
								email="rubia"
								lattesLink="http://lattes.cnpq.br/8086913547704538"
							/>

							<Linha
								nome="Silvio Luiz Bragatto Boss"
								link="/corpodocente/silvioBoss"
								titulacao="Mestre"
								regimedeTrabalho="DE"
								email="silvioboss"
								lattesLink="http://lattes.cnpq.br/5805926452305147"
							/>

							<Linha
								nome="Soelaine Rodrigues Ascari"
								link="/corpodocente/soelaineAscari"
								titulacao="Mestre"
								regimedeTrabalho="DE"
								email="soelaine"
								lattesLink="http://lattes.cnpq.br/7145298480843878"
							/>

							<Linha
								nome="Géri Natalino Dutra"
								link="/corpodocente/geriDutra"
								titulacao="Mestre"
								regimedeTrabalho="40h"
								email="natalino"
								lattesLink="http://lattes.cnpq.br/9921945692078074"
							/>

							<Linha
								nome="Richardson Ribeiro"
								link="/corpodocente/richardsonRibeiro"
								titulacao="Doutor"
								regimedeTrabalho="DE"
								email="richardsonr"
								lattesLink="http://lattes.cnpq.br/1732432214014006"
							/>      
				
							<div className="tituloUmaColuna"> <strong>Docentes substitutos</strong> </div>

							<Linha
								nome="----"
								link="#"
								titulacao="----"
								regimedeTrabalho="----"
								email="----"
								lattesLink=""
							/>   
						</div>
						
					</Col>
				</Row>
			</Container>
			<Footer />
		</div>
	);
}

export default CorpoDocente;