import React from "react"

import Navbar from "components/common/Navbar.js";
import Footer from "components/common/Footer.js";
import Leftbar from "components/common/Leftbar.js";
import Header from "components/home/Header.js";
import { Container, Row, Col } from 'reactstrap';
//import {Link} from "react-router-dom"
import "assets/css/custom.css"


function PaginaModelo (props){
	return (
		<div className="edit">
			<Navbar />
			<Header />
			<Container>
				<Row>
					<Col lg='3' className='menuLateral'>
						<Leftbar/>
					</Col>
					<Col md='12' lg='9' className="colStyle">
						<h2><strong> Pós-Graduação </strong></h2>
						<hr/>
						<p className="colStyle">
						O Departamento Acadêmico de Informática é responsável por três cursos de pós-graduação lato-sensu (especialização), sendo: Banco de Dados, Redes de Computadores e Tecnologia Java.
						</p>
						<br/>
						<a className="hiperLink" href="http://www.pb.utfpr.edu.br/redes/">Redes de Computadores</a>
						<br/>
						<br/>
						<a className="hiperLink" href="http://www.pb.utfpr.edu.br/posjava/index.html">Tecnologia Java</a>
						<br/>
						<br/>
						<a className="hiperLink" href="http://www.utfpr.edu.br/cursos/especializacao/pb/banco-de-dados">Banco de Dados</a>
						
					</Col>
				</Row>
			</Container>
			<Footer />
		</div>
	);
}

export default PaginaModelo;