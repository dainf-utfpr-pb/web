import React from "react";

// core components
import Navbar from "components/common/Navbar.js";
import Leftbar from "components/common/Leftbar.js";
import Footer from "components/common/Footer.js";
import Header from "components/home/Header.js";

import { Container, Row, Col } from "reactstrap"

import PagesRefs from "components/pub/Pages.js"

import Page0 from "./0/Page0.js"

function Noticias(props) {
	return (
		<div className="edit">
			<Navbar />
			<Header />
			<Container>
				<Row>
					<Col lg='3' className='menuLateral'>
						<Leftbar/>
					</Col>
					<Col md='12' lg='9' >
						<br/>
						<Page0 />
						<PagesRefs	//Escrever a ordem direta. A ultima pagina aparecerá primeiro (Ver PageRefs.js)
							refs = {["0"]}
							links = {[
								"/noticias/0"
							]}
						/>
					</Col>
				</Row>
			</Container>
			<Footer />
		</div>
	);
}

export default Noticias;
