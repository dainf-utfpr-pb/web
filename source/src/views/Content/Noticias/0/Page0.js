import React from "react"

//Caixa de estilo
import NewsBox from "components/pub/NewsBox.js"

//Notícia de exemplo

var Exemplo = [
	{...require("./exemplo0/Info.js")}
]

function LastNews () {
	return (
		<>
			{refer()}
		</>
	);
	function refer (){
		let lista = []
		for(let i = 0; i < Exemplo.length; i++){
			lista.push(
				<NewsBox
                  		title = {Exemplo[i].Title}
                  		subtitle = {Exemplo[i].Description}
                  		img = { require("./exemplo"+[i]+"/img") }
                  		date = {Exemplo[i].Date}
                  		author = {Exemplo[i].Author}
                  		text = {Exemplo[i].Text}
            		/>
			)
		}
		return lista
	}
}

export default LastNews;