export const Title = "Testando a Box";
export const Description = "Você sabe o que é liguagem fática?";
export const Date = "24-03-2020";
export const Author = "Allan Ribeiro";
export const Ref = "Titulos titulantes"
export const Link = "/noticias"
export var Text = "Acima de tudo, é fundamental ressaltar que a execução dos pontos do programa "+
"ainda não demonstrou convincentemente que vai participar na mudança das "+
"regras de conduta normativas. Podemos já vislumbrar o modo pelo qual o "+
"comprometimento entre as equipes cumpre um papel essencial na formulação do "+
"remanejamento dos quadros funcionais. No entanto, não podemos esquecer que "+
"a revolução dos costumes maximiza as possibilidades por conta dos procedimentos "+
"normalmente adotados. Caros amigos, a expansão dos mercados mundiais auxilia "+
"a preparação e a composição do levantamento das variáveis envolvidas.";