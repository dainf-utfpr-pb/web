export const Title = "Demonstação 1"
export const Description = "Um titulo deveras interessante";
export const Date = "24-03-2020";
export const Author = "Allan Ribeiro";
export const Ref = "Saber mais"
export const Link = "/noticias"
export var Text = "O empenho em analisar a consulta aos diversos"+
" militantes nos obriga à análise da gestão inovadora da qual fazemos"+
" parte. É claro que o consenso sobre a necessidade de qualificação"+
" estimula a padronização das diretrizes de desenvolvimento para o futuro."+
" Assim mesmo, o fenômeno da Internet maximiza as possibilidades por conta"+
" de alternativas às soluções ortodoxas. No entanto, não podemos esquecer"+
" que o novo modelo estrutural aqui preconizado exige a precisão e a"+
" definição das posturas dos órgãos dirigentes com relação às suas"+
" atribuições. Gostaria de enfatizar que a percepção das dificuldades"+
" garante a contribuição de um grupo importante na determinação das"+
" formas de ação. Nunca é demais lembrar o peso e o significado destes"+
" problemas, uma vez que o surgimento do comércio virtual assume importantes"+
" posições no estabelecimento dos conhecimentos estratégicos para atingir"+
" a excelência. Ainda assim, existem dúvidas a respeito de como a"+
" complexidade dos estudos efetuados ainda não demonstrou convincentemente"+
" que vai participar na mudança do sistema de participação geral."+
" Percebemos, cada vez mais, que o aumento do diálogo entre os"+
" diferentes setores produtivos obstaculiza a apreciação da importância"+
" do levantamento das variáveis envolvidas."
