export const Title = "Outro teste de box";
export const Description = "Vi sitter här i venten och spelar lite dota";
export const Date = "24-03-2020";
export const Author = "Allan Ribeiro";
export const Ref = "(i hear your man)"
export const Link = "/noticias"
export var Text = "Pensando mais a longo prazo, a crescente influência da mídia pode nos "+
"levar a considerar a reestruturação das condições inegavelmente apropriadas. "+
"Todavia, a contínua expansão de nossa atividade não pode mais se dissociar "+
"das novas proposições. Gostaria de enfatizar que a revolução dos costumes "+
"cumpre um papel essencial na formulação dos paradigmas corporativos. Podemos "+
"já vislumbrar o modo pelo qual a hegemonia do ambiente político prepara-nos "+
"para enfrentar situações atípicas decorrentes de todos os recursos funcionais envolvidos.";