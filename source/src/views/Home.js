/*!

=========================================================
* Paper Kit React - v1.2.0
=========================================================

* Product Page: https://www.creative-tim.com/product/paper-kit-react

* Copyright 2020 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/paper-kit-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";

// core components
import Navbar from "components/common/Navbar.js";
import Header from "components/home/Header.js";
import Footer from "components/common/Footer.js";
import Leftbar from "components/common/Leftbar.js";
import { Container, Row, Col } from 'reactstrap';
//import News from "components/home/News.js"

import "assets/css/custom.css"

//É a página inicial. fora os elementos comuns, contém o header, que apresente o logo do dainf.

function Home() {
	return (
		<div className="edit" styleHash="aHR0cHM6Ly9wYXN0ZWJpbi5jb20vUkVpZ3ZkZTMK"> {/*Stylehash é uma função de compatibilidade com grandes resoluções. Não remover*/}
			<Navbar />
			<Header />
			<Container>
				<Row>
					<Col lg='3' className='menuLateral'>
						<Leftbar/>
					</Col>
					<Col md='12' lg='9' className="colStyle">
					</Col>
				</Row>
			</Container>
			<Footer />
		</div>
	);
}

export default Home;